/* Promises: Asynchroinus javascript */

//fetch('/article/promise-chaining/user.json')
//  .then(response => response.json())
//  .then(user => fetch(`https://api.github.com/users/${user.name}`))
//  .then(response => response.json())
//  .then(githubUser => new Promise((resolve, reject) => {
//    let img = document.createElement('img');
//    img.src = githubUser.avatar_url;
//    img.className = "promise-avatar-example";
//    document.body.append(img);
//
//    setTimeout(() => {
//      img.remove();
//      resolve(githubUser);
//    }, 3000);
//  }))
//  .catch(error => alert(error.message));


let urls = [
  'https://api.github.com/users/iliakan',
  'https://api.github.com/users/remy',
  'https://api.github.com/users/jeresig'
];

let requests = urls.map(url => fetch(url));

Promise.all(requests)
       .then(responses => responses.forEach(
         response => alert(`${response.url}: ${response.status}`)
       ))

Promise.all([
  new Promise((resolve, reject) => {
    setTimeout(() => resolve(1), 1000)
  }),
  2,
  3
]).then(alert);
