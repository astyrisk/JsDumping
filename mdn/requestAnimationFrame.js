// requestAnimationFrame(callback)
// cancelAnimationFrame(callback)

const element = document.getElementsByClassName("a7a");
let start, previousTimeStamp;
let done = false;

function step(timestamp) {
    if (start === undefined) start = timestamp;
    const elapesd = timestamp - start;
    if (previousTimeStamp !== timestamp) {
        const cnt = Math.min(0.1 * elapesd, 200) ;
        element.style.transform = `translateX(${cnt}px)`;
        if (cnt === 200) done = true;
    }

    if (elapesd < 2000) {
        previousTimeStamp = timestamp;
        if (!done) window.requestAnimationFrame(step);
    }
}

window.requestAnimationFrame(step);
