//const results = [
//    {name: "Statisied", count: 1043, color: "lightblue"},
//    {name: "Neutral", count: 563, color: "lightgreen"},
//    {name: "Unsatisfied", count: 510, color: "pink"},
//    {name: "No comment", count: 175, color: "silver"}
//];

let cx = document.querySelector("canvas").getContext("2d");
//cx.beginPath();

//cx.strokeStyle = "blue";
//cx.strokeRect(5, 5, 50, 50);
//cx.lineWidth = 5;
//cx.strokeRect(135, 5, 50, 50);


//for (let y = 10; y < 100; y+= 10) {
//    cx.moveTo(10, y);
//    cx.lineTo(90, y);
//}

//cx.moveTo(50, 10);
//cx.lineTo(10, 70);
//cx.lineTo(70, 70);
//cx.fillStyle = "green";
//cx.fill();

//cx.moveTo(10, 90);
//cx.quadraticCurveTo(60, 10, 90, 90);
//cx.lineTo(60, 10);
//cx.closePath();
//cx.fill();

//let total = results.reduce((sum, {count}) => sum + count, 0);
//let currentAngle = - 0.5 * Math.PI;
//for (let result of results) {
//    let sliceAngle = ((result.count) / total) * 2 * Math.PI;
//    // BEGINPATH
//    cx.beginPath();
//    cx.arc(100, 100, 100, currentAngle, currentAngle += sliceAngle);
//    cx.lineTo(100, 100);
//    cx.fillStyle = result.color;
//    cx.fill();
//}

//cx.fillStyle = "red";
//cx.font = "48px Georgia";
//cx.fillText("I can draw text, too", 30, 50);

/* IMAGES */

//let img = document.createElement("img");
//img.src = "./img/player.png";
//const spriteW = 24, spriteH = 30;
//img.addEventListener("load", () => {
//    let cycle = 0;
//    setInterval(() => {
//    cx.clearRect(0, 0, spriteW, spriteH);
//    cx.drawImage(img,
//                cycle * spriteW, 0, spriteW, spriteH,
//                0,               0, spriteW, spriteH);
//    cycle = (cycle + 1) % 8;
//    }, 120);
//});

/* TRANSFORMATION */
//cx.scale(3, 1);
//cx.beginPath();
//cx.arc(50, 50, 40, 0, 7);
//cx.lineWidth = 3;
//cx.stroke();
//
//* HAB */
//img.addEventListener("load", () => {
//    flipHorizontally (cx, 100 +spriteW / 2);
//    cx.drawImage(img, 0, 0, spriteW, spriteH,
//                 100, 0, spriteW, spriteH);
//});
//
//function flipHorizontally(context, around) {
//    context.translate(around, 0);
//    context.scale(-1, 1);
//    context.translate(-around, 0);
//}
//

function branch(length, angle, scale) {
    cx.fillRect(0, 0, 0.9, length);
    if (length < 6)  return;
    cx.save();
    cx.translate(0, length);
    cx.rotate(-angle); 
    branch(length * scale, angle, scale)
    cx.rotate(2 * angle);
    branch(length * scale, angle, scale)
    cx.restore();
}
cx.translate(400, 0);
branch(60, 0.1, 0.8);
