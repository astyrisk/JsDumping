let cx = document.querySelector("canvas").getContext("2d");

let lastTime = null;
function frame(time) {
    if (lastTime != null) updateAnimation(Math.min(100, time - lastTime) /1000);
    lastTime = time;
    requestAnimationFrame(frame);
}
requestAnimationFrame(frame);

let x = 40, y = 50
let radius = 10;
let speedX = 40, speedY = 60;

function updateAnimation(step) {
    cx.clearRect(0, 0, 700, 700);
    cx.strokeStyle = "blue";
    cx.strokeRect(25, 25, 430, 230);

    x += step * speedX;
    y += step * speedY;
    if (x < 25 + radius || x > 455- radius) speedX = -speedX;
    if (y < 25 + radius || y > 255 - radius) speedY = -speedY;
    cx.beginPath();
    cx.arc(x, y, radius, 0, 7);
    cx.fill();
}

