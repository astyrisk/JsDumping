/* crows' JS : EJ 11th chapter */

/* crow-tech */

const connections = [
  "Church Tower-Sportsgrounds", "Church Tower-Big Maple", "Big Maple-Sportsgrounds",
  "Big Maple-Woods", "Big Maple-Fabienne's Garden", "Fabienne's Garden-Woods",
  "Fabienne's Garden-Cow Pasture", "Cow Pasture-Big Oak", "Big Oak-Butcher Shop",
  "Butcher Shop-Tall Poplar", "Tall Poplar-Sportsgrounds", "Tall Poplar-Chateau",
  "Chateau-Great Pine", "Great Pine-Jacques' Farm", "Jacques' Farm-Hawthorn",
  "Great Pine-Hawthorn", "Hawthorn-Gilles' Garden", "Great Pine-Gilles' Garden",
  "Gilles' Garden-Big Oak", "Gilles' Garden-Butcher Shop", "Chateau-Butcher Shop"
]

function storageFor(name) {
  let storage = Object.create(null)
  storage["food caches"] = ["cache in the oak", "cache in the meadow", "cache under the hedge"]
  storage["cache in the oak"] = "A hollow above the third big branch from the bottom. Several pieces of bread and a pile of acorns."
  storage["cache in the meadow"] = "Buried below the patch of nettles (south side). A dead snake."
  storage["cache under the hedge"] = "Middle of the hedge at Gilles' garden. Marked with a forked twig. Two bottles of beer."
  storage["enemies"] = ["Farmer Jacques' dog", "The butcher", "That one-legged jackdaw", "The boy with the airgun"]
  if (name == "Church Tower" || name == "Hawthorn" || name == "Chateau")
    storage["events on 2017-12-21"] = "Deep snow. Butcher's garbage can fell over. We chased off the ravens from Saint-Vulbas."
  let hash = 0
  for (let i = 0; i < name.length; i++) hash += name.charCodeAt(i)
  for (let y = 1985; y <= 2018; y++) {
    storage[`chicks in ${y}`] = hash % 6
    hash = Math.abs((hash << 2) ^ (hash + y))
  }
  if (name == "Big Oak") storage.scalpel = "Gilles' Garden"
  else if (name == "Gilles' Garden") storage.scalpel = "Woods"
  else if (name == "Woods") storage.scalpel = "Chateau"
  else if (name == "Chateau" || name == "Butcher Shop") storage.scalpel = "Butcher Shop"
  else storage.scalpel = "Big Oak"
  for (let prop of Object.keys(storage)) storage[prop] = JSON.stringify(storage[prop])
  return storage
}

class Network {
  constructor(connections, storageFor) {
    let reachable = Object.create(null)
    for (let [from, to] of connections.map(conn => conn.split("-"))) {
      ;(reachable[from] || (reachable[from] = [])).push(to)
      ;(reachable[to] || (reachable[to] = [])).push(from)
    }
    this.nodes = Object.create(null)
    for (let name of Object.keys(reachable))
      this.nodes[name] = new Node(name, reachable[name], this, storageFor(name))
    this.types = Object.create(null)
  }

  defineRequestType(name, handler) {
    this.types[name] = handler
  }

  everywhere(f) {
    for (let node of Object.values(this.nodes)) f(node)
  }
}

const $storage = Symbol("storage"), $network = Symbol("network")

function ser(value) {
  return value == null ? null : JSON.parse(JSON.stringify(value))
}

class Node {
  constructor(name, neighbors, network, storage) {
    this.name = name
    this.neighbors = neighbors
    this[$network] = network
    this.state = Object.create(null)
    this[$storage] = storage
  }

  send(to, type, message, callback) {
    let toNode = this[$network].nodes[to]
    if (!toNode || !this.neighbors.includes(to))
      return callback(new Error(`${to} is not reachable from ${this.name}`))
    let handler = this[$network].types[type]
    if (!handler)
      return callback(new Error("Unknown request type " + type))
    if (Math.random() > 0.03) setTimeout(() => {
      try {
        handler(toNode, ser(message), this.name, (error, response) => {
          setTimeout(() => callback(error, ser(response)), 10)
        })
      } catch(e) {
        callback(e)
      }
    }, 10 + Math.floor(Math.random() * 10))
  }

  readStorage(name, callback) {
    let value = this[$storage][name]
    setTimeout(() => callback(value && JSON.parse(value)), 20)
  }

  writeStorage(name, value, callback) {
    setTimeout(() => {
      this[$storage][name] = JSON.stringify(value)
      callback()
    }, 20)
  }
}

let network = new Network(connections, storageFor);
let bigOak = network.nodes["Big Oak"];
let everywhere = network.everywhere.bind(network);
let defineRequestType = network.defineRequestType.bind(network);


/* crows: a crow's network */
// CallBack functions

/*
bigOak.readStorage("food caches", caches => { // CallbackBased function
  let firstCache = caches[0];
  bigOak.readStorage(firstCache, info => {
    console.log(info);
  });
});
*/

// bigOak.send("Cow Pasture", "note", "Let's caw loudly at 7PM", () => console.log("Note delivered."));

defineRequestType("note", (nest, content, source, done) => {
  console.log(`${nest.name} received note: ${content}`);
  done();
});

bigOak.send("Cow Pasture", "note", "Let's caw loudly at 7PM", () => console.log("Note delivered."));
// Promises

let fifteen = Promise.resolve(15);
// fifteen.then(value => console.log(`Got ${value}`));

function storage(nest, name) {
  return new Promise(resolve => {
    nest.readStorage(name, result => resolve(result));
  });
}


// Promise.reject = new rejected promise
storage(bigOak, "enemies").then(value => console.log("Enemies: ", value));

// failures

/*
new Promise((_, reject) => reject(new Error("Fail")))
  .then(value => console.log("Handler 1"))
  .catch( reason => {
    console.log("Caught failure " + reason);
    return "nothing";
  })
  .then(value => console.log("Handler 2", value));
*/

class Timeout extends Error {}

function request(nest, target, type, content) {
  return new Promise((resolve, reject) => {
    let done = false;
    function attempt(n) {
      nest.send(target, type, content, (failed, value) => {
        done = true;
        if (failed) reject(failed);
        else resolve(value);
      });
      setTimeout(() => {
        if (done) return;
        else if (n < 3) attempt(n + 1);
        else reject(new Timeout("Timed out"));
      }, 250);
    }
    attempt(1);
  });
}

function requestType(name, handler) {
  defineRequestType(name, (nest, content, source, callback) => {
    try{
      Promise.resolve(handler(nest, content, source))
        .then(response => callback(null, response), failure => callback(failure));
    } catch (ex) {
      callback(ex);
    }
  });
}

requestType("ping", () => "pong");

function availabeNeighbors(nest) {
  let requests = nest.neighbors.map(neighbor => {
    return request(nest, neighbor, "ping")
      .then(() => true, () => false);
  });

  return Promise.all(requests).then(result => {
    return nest.neighbors.filter((_, i) => result[i]);
  });
}

// console.log(availabeNeighbors(bigOak));

// NETWORK FLOODING
everywhere(nest => {
  nest.state.gossip = [];
});

function sendGossip(nest, message, exceptFor = null) {
  nest.state.gossip.push(message);
  for (let neighbor of nest.neighbors) {
    if (neighbor == exceptFor) continue;
    request(nest, neighbor, "gossip", message);
  }
}

requestType("gossip", (nest, message, source) => {
  if (nest.state.gossip.includes(message)) return;
  console.log(`${nest.name} received gossip '${message}' from ${source}`);
  sendGossip(nest, message, source);
});


// sendGossip(bigOak, "Kids with airgun in the park");

// MESSAGE ROUTING

requestType("connections", (nest, {name, neighbors}, source) => {
  let connections = nest.state.connections;
  if (JSON.stringify(connections.get(name)) == JSON.stringify(neighbors)) return;
  connections.set(name, neighbors);
  broadcastConnections(nest, name, source);
});

function broadcastConnections(nest, name, exceptFor = null) {
  for (let neighbor of nest.neighbors) {
    if (neighbor == exceptFor) continue;
    request (nest, neighbor, "connections", {
      name,
      neighbors: nest.state.connections.get(name)
    });
  }
}

everywhere(nest => {
  nest.state.connections = new Map();
  nest.state.connections.set(nest.name, nest.neighbors);
  broadcastConnections(nest, nest.name);
});

function findRoute(from, to, connections) {
  let work = [{at: from, via: null}];
  for (let i = 0; i < work.length; i++) {
    let {at, via} = work[i];
    for (let next of connections.get(at) || []) {
      if (next == to) return via;
      if (!work.some(w => w.at == next)) {
        work.push({at: next, via: via || next});
      }
    }
  }
  return null;
}

function routeRequest(nest, target, type, content) {
  if (nest.neighbors.includes(target)) {
    return request(nest, target, type, content);
  } else {
    let via = findRoute(nest.name, target,
                        nest.state.connections);
    if (!via) throw new Error(`No route to ${target}`);
    return request(nest, via, "route",
                   {target, type, content});
  }
}

requestType("route", (nest, {target, type, content}) => {
  return routeRequest(nest, target, type, content);
});

// routeRequest(bigOak, "Church Tower", "note", "Incoming jackdaws!");

requestType("storage", (nest, name) => storage(nest, name));

//function findInStorage(nest, name) {
//  return storage(nest, name).then(found => {
//    if (found != null) return found;
//    else return findInRemoteStorage(nest, name);
//  });
//}
//

function network2(nest) {
  return Array.from(nest.state.connections.keys());
}

//
//function findInRemoteStorage(nest, name) {
//  let sources = network2(nest).filter(n => n != nest.name);
//
//  function next() {
//    if (sources.length == 0) {
//      return Promise.reject(new Error("Not found"));
//    } else {
//      let source = sources[Math.floor(Math.random() * sources.length)];
//      sources = sources.filter(n => n != source);
//      return routeRequest(nest, source, "storage", name).then(value => value != null ? value : next(), next);
//    }
//  }
//
//  return next();
//}

async function findInStorage(nest, name) {
  let local = await storage(nest, name);
  if (local != null) return local;

  let sources = network2(nest).filter(n => n != nest.name);
  while (sources.length) {
    let source = sources[Math.floor(Math.random() * sources.length)];
    sources = sources.filter(n => n != source);

    try {
      let found = await routeRequest(nest, source, "storage", name);
      if (found != null) return found;
    } catch(_) {}
  }
  throw new Error("Not found");
}

findInStorage(bigOak, "events on 2017-12-21").then(console.log);

function* powers(n) {
  for (let current = n;; current *= n) {
    yield current;
  }
}

for (let power of powers(3)) {
  if (power > 50) break;
  console.log(power);
}

function anyStorage(nest, source, name) {
  if (source == nest.name) return storage(nest, name);
  else return routeRequest(nest, source, "storage", name);
}

/* Exercises */

async function  locateScalpel(nest) {
  let current = nest.name;
  for (;;) {
    let next = await anyStorage(nest, current, "scalpel");
    if (next == current) return current;
    current = next;
  }
}

function locateScalpel2(nest) {
  function loop(current) {
    return anyStorage(nest, current, "scalpel").then(next => {
      if (next == current) return current;
      else return loop(next);
    });
  }
  loop(nest.name);
}

locateScalpel2(bigOak).then(console.log);
