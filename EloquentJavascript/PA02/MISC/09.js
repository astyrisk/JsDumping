/* chapter 09: Regular Expressions */

// playground

let re1 = new RegExp("abc");
let re2 = /abc/;

let match = /\d+/.exec("one two 109");
console.log(match);

let quotedText = /'([^']*)'/;
console.log(quotedText.exec("she said 'hello'"));

console.log(new Date(2009, 11, 9));  // Decemeber: Month numbers start at zero 

let UnixTime = new Date(1970, 0, 1);
console.log(UnixTime.getTime); // prints the function itself
console.log(UnixTime.getTime()); // prints the value

function getDate(string) {
    let [_, month, day, year] =
        /(\d{1,2})-(\d{1,2})-(\d{4})/.exec(string);
    return new Date(year, month - 1, day);
}

console.log(getDate("1-30-2023"));
console.log(/hello?l/.test("hello"));
console.log("papa".replace("p", "m"));

console.log(/\da(b)*c/.test("fkjs 9apc lmao"));
console.log(/(0|1)+/.test("19"));
console.log(/^.*x/.test("abcsdhfx"))

let BinaryRegEx = /([01]+)+b/;
console.log("Final:\t", BinaryRegEx.exec("01000b"));

console.log("Bubo") /* without replacing */
console.log("Bubo".replace(/[ou]/, "a"));
console.log("Bubo".replace(/[ou]/g, "a"));

function stripComments(code) {
    return code.replace(/\/\/.*|\/\*[^]*?\*\//g, "");
}

let name = "har";
let txt = "har is a lmaohar";
let regex = new RegExp("\\b(" + name + ")\\b", "gi");

let global = /abc/g;
console.log(global.exec("xyz abc"));

let pat = /xyz/g;

let input = "A string with 3 number int it.. 42 and 88.";
let number = /\b\d+\b/g;
let matjkjch;

while (match = number.exec(input)) {
    console.log("Found", match[0], "at", match.index);
}

function parseINI(string) { /* paresINT ??? */
    let res = {};
    let section = res;

    string.split(/\r?\n/).forEach(line => { /* \r?\n  for linux and windows, but I hate windows */
        let match;
        if (match = line.match(/^(\w+)=(.*)$/)) {
            console.log(match);
            section[match[1]] = match[2];
        } else if (match = line.match(/^\[(.*)\]$/)) {
            section = res[match[1]] = {};
        } else if (!/^\s*(;.*)?$/.test(line)) {
            throw new Error("Line '" + line + "' is not valid.'");
        }
   });
    return res;
}

console.log(parseINI(
`name=vasilis
[address]
city=Tessaloniki`
));


// exercises
