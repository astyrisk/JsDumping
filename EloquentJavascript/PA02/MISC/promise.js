const handleResolved =  (a) => { 
    console.log("resolvedA: ", a);
}

const handleRejected =  () => { 
    console.log("rejectedA");
}

const myPromise = new Promise((resolve, _) => { 
    setTimeout(() => { 
        resolve('foo');
    }, 300);
});

myPromise.then(handleResolved, handleRejected);
