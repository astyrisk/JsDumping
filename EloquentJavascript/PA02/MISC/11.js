/* old days */

let fif = Promise.resolve(15);
fif.then(val => console.log(`Got ${val}`));


class Timeout extends Error {}

function request(nest, target, type, content) {
    return new Promise((resolve, reject) => {
        let done = false;
        function attempt(n) {
            nest.send(target, type, content, (failed, value) => {
                done = true;
                if (failed) reject(failed);
                else resolve(value);
            });
            setTimeout(() => {
                if (done) return;
                else if (n < 3) attempt(n + 1);
                else reject(new Timeout("Timed out"));
            }, 250)
        }
        attempt(1);
    });
}

let poo = (m) => 5;
let loo = (l, n) => {
    console.log("javaScript");
    if(n) { console.log(n); }; // it works
}
let loo2 = (l, n) => {
    console.log("javaScript");
    console.log(n);
}

loo(1, poo());

function successCallack(result) {
    console.log("Audio file ready at url:", result);
}

function failureCallback(error) {
    console.error("Error generating audio file: " + error);
}

/* catch(failureCallback) = then(null, failureCallback) */

new Promise((_, reject) => reject)
    .then(value => console.log("heeeeeeey2"));


// createAudioFileAsync(audioSettings, successCallack, failureCallback);

/* then returns a new promise different from the original */

new Promise((_, reject) => reject(new Error("Fail")))
    .then(value => console.log("Handler 1"))
    .catch(reason => { 
        console.log("Caught failure: " + reason);
        return "nothing";
    })
    .then(value => console.log("handler 2", value));

/* crow  */
