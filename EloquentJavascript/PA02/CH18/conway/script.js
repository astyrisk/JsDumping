/* conway's game of life */

// (alive) fewer than two, more than three => dies
// (alive) two or three neighbors => lives
// (dead) three neighbors => lives

const WIDTH_MAX = 35, HEIGHT_MAX = 20;
let parentN = document.querySelector("#parent");
let gridNode = document.querySelector("#grid");
let btn = document.querySelector("#run");
let nextBtn = document.querySelector("#next");
let ARR_DEF = [];

const randomBool = () => {
  return (Math.random() >= 0.87);
}


for (let y = 0; y < HEIGHT_MAX; y++) {
  for (let x = 0; x < WIDTH_MAX; x++) {
    let box = document.createElement("input");
    box.type = "checkbox";
    gridNode.appendChild(box);
    ARR_DEF.push(box);
  }
  gridNode.appendChild(document.createElement("br"));
}

const propagate = (func, arr, index) => {
  for (let y = 0; y < HEIGHT_MAX; y++)
    for (let x = 0; x < WIDTH_MAX; x++)
      func(arr[(y*WIDTH_MAX) + x], index);
}

const random = (c) => {
  if (randomBool()) c.checked = true;
}

const neighbors = (arr, i) => {
  arr[i].neighbors = getNeighbors(i, arr);
}

/* position */
const isTop = (i) => (i <= (WIDTH_MAX - 1));
const isBot = (i) => (i >= ((HEIGHT_MAX-1) * WIDTH_MAX));
const isLeft = (i) => (i % WIDTH_MAX) == 0;
const isRight = (i) => ((i+1) % WIDTH_MAX) == 0;

const getRight = (index, arr) => arr[index+1];
const getLeft = (index, arr) => arr[index-1];
const getTheTwosButCenter = (index, arr) => [arr[index - 1], arr[index + 1]];
const getTheTwosButLeft = (index, arr) => [arr[index], arr[index + 1]];
const getTheTwosButRight = (index, arr) => [arr[index], arr[index - 1]];
const getTheThrees = (index, arr) => [arr[index], arr[index - 1], arr[index+1]];
/* for normalCells, top, bot cell*/
const getNormNeighbors = (index, arr) =>  [...getTheTwosButCenter(index, arr), ...getTheThrees(index - WIDTH_MAX, arr), ...getTheThrees(index + WIDTH_MAX, arr)].filter(x => x != undefined);
/* bc of fucking corners */

const getNeighbors = (index, arr) => {
  if (isLeft(index)) {
    if (isTop(index))      return [getRight(index, arr),   ...getTheTwosButLeft(index + WIDTH_MAX, arr)];
    else if (isBot(index)) return [getRight(index, arr),   ...getTheTwosButLeft(index - WIDTH_MAX, arr)];
    else                   return [getRight(index, arr),   ...getTheTwosButLeft(index - WIDTH_MAX, arr), ...getTheTwosButLeft(index + WIDTH_MAX, arr)];
  }
  else if (isRight(index)) {
    if (isTop(index))      return [getLeft(index,  arr),   ...getTheTwosButRight((index + WIDTH_MAX), arr)];
    else if (isBot(index)) return [getLeft(index,  arr),   ...getTheTwosButRight((index - WIDTH_MAX), arr)];
    else                   return [getLeft(index,  arr),   ...getTheTwosButRight(index - WIDTH_MAX, arr), ...getTheTwosButRight(index + WIDTH_MAX, arr)];
  }
  else return (getNormNeighbors(index, arr));
}

const nextArray = () => {
  for (let i = 0; i < (WIDTH_MAX * HEIGHT_MAX); i++) {
    let cnt = getNeighbors(i, ARR_DEF).filter(x => x.checked).length;
    if (ARR_DEF[i].checked) {
      if (cnt > 3 || cnt < 2) ARR_DEF[i].checked = false;
    } else {
      if (cnt == 3) ARR_DEF[i].checked = true;
    }
  }
}

propagate(random, ARR_DEF);

let running = null;
btn.addEventListener("click", () => {
  if (running) {
    clearInterval(running);
    running = null;
  } else {
    running = setInterval(nextArray, 400);
  }
});

btn.addEventListener("click", nextArray);
